package ua.denysyashchuk.englishforchildren;

/**
 * This class represents word element, which include english and local translation of the word,
 * image and audio.
 */
public class Word {

    private int englishTranslation;
    private int localTranslation;
    private int imageResourseId;
    private int audioResourseId;

    public Word(int englishTranslation, int localTranslation, int imageResourseId, int audioResourseId) {
        this.englishTranslation = englishTranslation;
        this.localTranslation = localTranslation;
        this.imageResourseId = imageResourseId;
        this.audioResourseId = audioResourseId;
    }

    public int getEnglishTranslation() {
        return englishTranslation;
    }

    public int getLocalTranslation() {
        return localTranslation;
    }

    public int getAudioResourseId(){ return audioResourseId; }

    public int getImageResourseId() {
        return imageResourseId;
    }

}
