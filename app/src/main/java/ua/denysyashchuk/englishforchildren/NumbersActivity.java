package ua.denysyashchuk.englishforchildren;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;


/**
 * Activity that represents English numbers, its local translation, the image, which is connected
 * to this word and gives possibility for listening english pronunciation of this word
 */
public class NumbersActivity extends AppCompatActivity {


    MediaPlayer mp;
    private AudioManager am;

    AudioManager.OnAudioFocusChangeListener afChangeListener = new AudioManager.OnAudioFocusChangeListener() {
        @Override


        public void onAudioFocusChange(int focusChange) {
            if (focusChange == AudioManager.AUDIOFOCUS_LOSS_TRANSIENT ||
                    focusChange == AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK) {
                //The AUDIOFOCUS_LOSS_TRANSIENT case means we lost focus for short time and
                // Pause playback and seek audio to the start position
                mp.pause();
                mp.seekTo(0);

            } else if (focusChange == AudioManager.AUDIOFOCUS_GAIN) {
                // The AUDIOFOCUS_GAIN case means we have regained focus and can resume playback.
                mp.start();

            } else if (focusChange == AudioManager.AUDIOFOCUS_LOSS) {
                // The AUDIOFOCUS_LOSS case means we've lost audio focus and
                // Stop playback and clean up resources
                releaseMediaPlayer();
            }

        }
    };

    private MediaPlayer.OnCompletionListener completionListener = new MediaPlayer.OnCompletionListener() {
        @Override
        public void onCompletion(MediaPlayer mediaPlayer) {
            // Now that the sound file has finished playing, release the media player resources.
            releaseMediaPlayer();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.word_activity);

        // creates a list of words
        final ArrayList<Word> words = new ArrayList<Word>();
        words.add(new Word(R.string.one_english, R.string.one_local, R.drawable.number_one, R.raw.number_one));
        words.add(new Word(R.string.two_english, R.string.two_local, R.drawable.number_two, R.raw.number_two));
        words.add(new Word(R.string.three_english, R.string.three_local, R.drawable.number_three, R.raw.number_three));
        words.add(new Word(R.string.four_english, R.string.four_local, R.drawable.number_four, R.raw.number_four));
        words.add(new Word(R.string.five_english, R.string.five_local, R.drawable.number_five, R.raw.number_five));
        words.add(new Word(R.string.six_english, R.string.six_local, R.drawable.number_six, R.raw.number_six));
        words.add(new Word(R.string.seven_english, R.string.seven_local, R.drawable.number_seven, R.raw.number_seven));
        words.add(new Word(R.string.eight_english, R.string.eight_local, R.drawable.number_eight, R.raw.number_eight));
        words.add(new Word(R.string.nine_english, R.string.nine_local, R.drawable.number_nine, R.raw.number_nine));
        words.add(new Word(R.string.ten_english, R.string.ten_local, R.drawable.number_ten, R.raw.number_ten));
        words.add(new Word(R.string.eleven_english, R.string.eleven_local, R.drawable.number_eleven, R.raw.number_eleven));
        words.add(new Word(R.string.twelve_english, R.string.twelve_local, R.drawable.number_twelve, R.raw.number_twelve));
        words.add(new Word(R.string.thirteen_english, R.string.thirteen_local, R.drawable.number_thirteen, R.raw.number_thirteen));
        words.add(new Word(R.string.fourteen_english, R.string.fourteen_local, R.drawable.number_fourteen, R.raw.number_fourteen));
        words.add(new Word(R.string.fifteen_english, R.string.fifteen_local, R.drawable.number_fifteen, R.raw.number_fifteen));
        words.add(new Word(R.string.sixteen_english, R.string.sixteen_local, R.drawable.number_sixteen, R.raw.number_sixteen));
        words.add(new Word(R.string.seventeen_english, R.string.seventeen_local, R.drawable.number_seventeen, R.raw.number_seventeen));
        words.add(new Word(R.string.eighteen_english, R.string.eighteen_local, R.drawable.number_eighteen, R.raw.number_eighteen));
        words.add(new Word(R.string.nineteen_english, R.string.nineteen_local, R.drawable.number_nineteen, R.raw.number_nineteen));
        words.add(new Word(R.string.twenty_english, R.string.twenty_local, R.drawable.number_twenty, R.raw.number_twenty));
        words.add(new Word(R.string.thirty_english, R.string.thirty_local, R.drawable.number_thirty, R.raw.number_thirty));
        words.add(new Word(R.string.forty_english, R.string.forty_local, R.drawable.number_forty, R.raw.number_forty));
        words.add(new Word(R.string.fifty_english, R.string.fifty_local, R.drawable.number_fifty, R.raw.number_fifty));
        words.add(new Word(R.string.sixty_english, R.string.sixty_local, R.drawable.number_sixty, R.raw.number_sixty));
        words.add(new Word(R.string.seventy_english, R.string.seventy_local, R.drawable.number_seventy, R.raw.number_seventy));
        words.add(new Word(R.string.eighty_english, R.string.eighty_local, R.drawable.number_eighty, R.raw.number_eighty));
        words.add(new Word(R.string.ninety_english, R.string.ninety_local, R.drawable.number_ninety, R.raw.number_ninety));
        words.add(new Word(R.string.hundred_english, R.string.hundred_local, R.drawable.number_hundred, R.raw.number_hundred));
        words.add(new Word(R.string.thousand_english, R.string.thousand_local, R.drawable.number_thousand, R.raw.number_thousand));
        words.add(new Word(R.string.million_english, R.string.million_local, R.drawable.number_million, R.raw.number_million));

        // creating word adapter
        WordAdapter itemsAdapter = new WordAdapter(this, words, R.color.category_numbers, R.color.numbers_image_background);
        ListView listView = (ListView) findViewById(R.id.words_list);
        listView.setAdapter(itemsAdapter);
        am = (AudioManager) this.getSystemService(Context.AUDIO_SERVICE);

        // set listener for playing music when item was clicked
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                int result = am.requestAudioFocus(afChangeListener,
                        AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN_TRANSIENT);
                if (result == AudioManager.AUDIOFOCUS_REQUEST_GRANTED) {
                    releaseMediaPlayer();
                    mp = MediaPlayer.create(NumbersActivity.this, words.get(position).getAudioResourseId());
                    mp.start();
                    mp.setOnCompletionListener(completionListener);
                }
            }

        });

    }

    @Override
    protected void onStop() {
        super.onStop();
        releaseMediaPlayer();
    }

    private void releaseMediaPlayer() {
        if (mp != null) {
            mp.release();
            mp = null;
            am.abandonAudioFocus(afChangeListener);
        }
    }
}
