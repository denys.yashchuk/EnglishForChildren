package ua.denysyashchuk.englishforchildren;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;


/**
 * Activity that represents English family members, its local translation, the image, which is connected
 * to this word and gives possibility for listening english pronunciation of this word
 */
public class FamilyActivity extends AppCompatActivity {


    MediaPlayer mp;
    private AudioManager am;

    AudioManager.OnAudioFocusChangeListener afChangeListener = new AudioManager.OnAudioFocusChangeListener() {
        @Override


        public void onAudioFocusChange(int focusChange) {
            if (focusChange == AudioManager.AUDIOFOCUS_LOSS_TRANSIENT ||
                    focusChange == AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK) {
                //The AUDIOFOCUS_LOSS_TRANSIENT case means we lost focus for short time and
                // Pause playback and seek audio to the start position
                mp.pause();
                mp.seekTo(0);

            } else if (focusChange == AudioManager.AUDIOFOCUS_GAIN) {
                // The AUDIOFOCUS_GAIN case means we have regained focus and can resume playback.
                mp.start();

            } else if (focusChange == AudioManager.AUDIOFOCUS_LOSS) {
                // The AUDIOFOCUS_LOSS case means we've lost audio focus and
                // Stop playback and clean up resources
                releaseMediaPlayer();
            }

        }
    };

    private MediaPlayer.OnCompletionListener completionListener = new MediaPlayer.OnCompletionListener() {
        @Override
        public void onCompletion(MediaPlayer mediaPlayer) {
            // Now that the sound file has finished playing, release the media player resources.
            releaseMediaPlayer();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.word_activity);

        // creates a list of words
        final ArrayList<Word> words = new ArrayList<Word>();
        words.add(new Word(R.string.family_english, R.string.family_local, R.drawable.family_family, R.raw.family_family));
        words.add(new Word(R.string.parents_english, R.string.parents_local, R.drawable.family_parents, R.raw.family_parents));
        words.add(new Word(R.string.mother_english, R.string.mother_local, R.drawable.family_mother, R.raw.family_mother));
        words.add(new Word(R.string.father_english, R.string.father_local, R.drawable.family_father, R.raw.family_father));
        words.add(new Word(R.string.sister_english, R.string.sister_local, R.drawable.family_sister, R.raw.family_sister));
        words.add(new Word(R.string.brother_english, R.string.brother_local, R.drawable.family_brother, R.raw.family_brother));
        words.add(new Word(R.string.daughter_english, R.string.daughter_local, R.drawable.family_daughter, R.raw.family_daughter));
        words.add(new Word(R.string.son_english, R.string.son_local, R.drawable.family_son, R.raw.family_son));
        words.add(new Word(R.string.grandmother_english, R.string.grandmother_local, R.drawable.family_grandmother, R.raw.family_grandmother));
        words.add(new Word(R.string.grandfather_english, R.string.grandfather_local, R.drawable.family_grandfather, R.raw.family_grandfather));
        words.add(new Word(R.string.child_english, R.string.child_local, R.drawable.family_child, R.raw.family_child));
        words.add(new Word(R.string.children_english, R.string.children_local, R.drawable.family_children, R.raw.family_children));

        // creating word adapter
        WordAdapter itemsAdapter = new WordAdapter(this, words, R.color.category_family, R.color.family_image_background);
        ListView listView = (ListView) findViewById(R.id.words_list);
        listView.setAdapter(itemsAdapter);
        am = (AudioManager) this.getSystemService(Context.AUDIO_SERVICE);

        // set listener for playing music when item was clicked
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                int result = am.requestAudioFocus(afChangeListener,
                        AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN_TRANSIENT);
                if (result == AudioManager.AUDIOFOCUS_REQUEST_GRANTED) {
                    releaseMediaPlayer();
                    mp = MediaPlayer.create(FamilyActivity.this, words.get(position).getAudioResourseId());
                    mp.start();
                    mp.setOnCompletionListener(completionListener);
                }
            }

        });


    }

    @Override
    protected void onStop() {
        super.onStop();
        releaseMediaPlayer();
    }

    private void releaseMediaPlayer() {
        if (mp != null) {
            mp.release();
            mp = null;
            am.abandonAudioFocus(afChangeListener);
        }
    }
}
