package ua.denysyashchuk.englishforchildren;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;


/**
 * Activity that represents English colors, its local translation, the image, which is connected
 * to this word and gives possibility for listening english pronunciation of this word
 */
public class ColorsActivity extends AppCompatActivity {


    MediaPlayer mp;
    private AudioManager am;

    AudioManager.OnAudioFocusChangeListener afChangeListener = new AudioManager.OnAudioFocusChangeListener() {
        @Override


        public void onAudioFocusChange(int focusChange) {
            if (focusChange == AudioManager.AUDIOFOCUS_LOSS_TRANSIENT ||
                    focusChange == AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK) {
                //The AUDIOFOCUS_LOSS_TRANSIENT case means we lost focus for short time and
                // Pause playback and seek audio to the start position
                mp.pause();
                mp.seekTo(0);

            } else if (focusChange == AudioManager.AUDIOFOCUS_GAIN) {
                // The AUDIOFOCUS_GAIN case means we have regained focus and can resume playback.
                mp.start();

            } else if (focusChange == AudioManager.AUDIOFOCUS_LOSS) {
                // The AUDIOFOCUS_LOSS case means we've lost audio focus and
                // Stop playback and clean up resources
                releaseMediaPlayer();
            }

        }
    };

    private MediaPlayer.OnCompletionListener completionListener = new MediaPlayer.OnCompletionListener() {
        @Override
        public void onCompletion(MediaPlayer mediaPlayer) {
            // Now that the sound file has finished playing, release the media player resources.
            releaseMediaPlayer();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.word_activity);

        // creates a list of words
        final ArrayList<Word> words = new ArrayList<Word>();
        words.add(new Word(R.string.red_english, R.string.red_local, R.drawable.color_red, R.raw.color_red));
        words.add(new Word(R.string.pink_english, R.string.pink_local, R.drawable.color_pink, R.raw.color_pink));
        words.add(new Word(R.string.orange_english, R.string.orange_local, R.drawable.color_orange, R.raw.color_orange));
        words.add(new Word(R.string.yellow_english, R.string.yellow_local, R.drawable.color_yellow, R.raw.color_yellow));
        words.add(new Word(R.string.green_english, R.string.green_local, R.drawable.color_green, R.raw.color_green));
        words.add(new Word(R.string.blue_english, R.string.blue_local, R.drawable.color_blue, R.raw.color_blue));
        words.add(new Word(R.string.purple_english, R.string.purple_local, R.drawable.color_purple, R.raw.color_purple));
        words.add(new Word(R.string.brown_english, R.string.brown_local, R.drawable.color_brown, R.raw.color_brown));
        words.add(new Word(R.string.black_english, R.string.black_local, R.drawable.color_black, R.raw.color_black));
        words.add(new Word(R.string.grey_english, R.string.grey_local, R.drawable.color_grey, R.raw.color_grey));
        words.add(new Word(R.string.white_english, R.string.white_local, R.drawable.color_white, R.raw.color_white));

        // creating word adapter
        WordAdapter itemsAdapter = new WordAdapter(this, words, R.color.category_colors, R.color.colors_image_background);
        ListView listView = (ListView) findViewById(R.id.words_list);
        listView.setAdapter(itemsAdapter);
        am = (AudioManager) this.getSystemService(Context.AUDIO_SERVICE);

        // set listener for playing music when item was clicked
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                int result = am.requestAudioFocus(afChangeListener,
                        AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN_TRANSIENT);
                if (result == AudioManager.AUDIOFOCUS_REQUEST_GRANTED) {
                    releaseMediaPlayer();
                    mp = MediaPlayer.create(ColorsActivity.this, words.get(position).getAudioResourseId());
                    mp.start();
                    mp.setOnCompletionListener(completionListener);
                }
            }

        });


    }

    @Override
    protected void onStop() {
        super.onStop();
        releaseMediaPlayer();
    }

    private void releaseMediaPlayer() {
        if (mp != null) {
            mp.release();
            mp = null;
            am.abandonAudioFocus(afChangeListener);
        }
    }
}
