package ua.denysyashchuk.englishforchildren;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;


/**
 * Activity that represents English animals, its local translation, the image, which is connected
 * to this word and gives possibility for listening english pronunciation of this word
 */
public class AnimalsActivity extends AppCompatActivity {


    MediaPlayer mp;
    private AudioManager am;

    AudioManager.OnAudioFocusChangeListener afChangeListener = new AudioManager.OnAudioFocusChangeListener() {
        @Override


        public void onAudioFocusChange(int focusChange) {
            if (focusChange == AudioManager.AUDIOFOCUS_LOSS_TRANSIENT ||
                    focusChange == AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK) {
                //The AUDIOFOCUS_LOSS_TRANSIENT case means we lost focus for short time and
                // Pause playback and seek audio to the start position
                mp.pause();
                mp.seekTo(0);

            } else if (focusChange == AudioManager.AUDIOFOCUS_GAIN) {
                // The AUDIOFOCUS_GAIN case means we have regained focus and can resume playback.
                mp.start();

            } else if (focusChange == AudioManager.AUDIOFOCUS_LOSS) {
                // The AUDIOFOCUS_LOSS case means we've lost audio focus and
                // Stop playback and clean up resources
                releaseMediaPlayer();
            }

        }
    };

    private MediaPlayer.OnCompletionListener completionListener = new MediaPlayer.OnCompletionListener() {
        @Override
        public void onCompletion(MediaPlayer mediaPlayer) {
            // Now that the sound file has finished playing, release the media player resources.
            releaseMediaPlayer();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.word_activity);

        // creates a list of words
        final ArrayList<Word> words = new ArrayList<Word>();
        words.add(new Word(R.string.bird_english, R.string.bird_local, R.drawable.animals_bird, R.raw.animals_bird));
        words.add(new Word(R.string.cat_english, R.string.cat_local, R.drawable.animals_cat, R.raw.animals_cat));
        words.add(new Word(R.string.chicken_english, R.string.chicken_local, R.drawable.animals_chicken, R.raw.animals_chicken));
        words.add(new Word(R.string.cock_english, R.string.cock_local, R.drawable.animals_cock, R.raw.animals_cock));
        words.add(new Word(R.string.dog_english, R.string.dog_local, R.drawable.animals_dog, R.raw.animals_dog));
        words.add(new Word(R.string.duck_english, R.string.duck_local, R.drawable.animals_duck, R.raw.animals_duck));
        words.add(new Word(R.string.elephant_english, R.string.elephant_local, R.drawable.animals_elephant, R.raw.animals_elephant));
        words.add(new Word(R.string.fox_english, R.string.fox_local, R.drawable.animals_fox, R.raw.animals_fox));
        words.add(new Word(R.string.frog_english, R.string.frog_local, R.drawable.animals_frog, R.raw.animals_frog));
        words.add(new Word(R.string.giraffe_english, R.string.giraffe_local, R.drawable.animals_giraffe, R.raw.animals_giraffe));
        words.add(new Word(R.string.hippopotamus_english, R.string.hippopotamus_local, R.drawable.animals_hippopotamus, R.raw.animals_hippopotamus));
        words.add(new Word(R.string.lion_english, R.string.lion_local, R.drawable.animals_lion, R.raw.animals_lion));
        words.add(new Word(R.string.monkey_english, R.string.monkey_local, R.drawable.animals_monkey, R.raw.animals_monkey));
        words.add(new Word(R.string.mouse_english, R.string.mouse_local, R.drawable.animals_mouse, R.raw.animals_mouse));
        words.add(new Word(R.string.penguin_english, R.string.penguin_local, R.drawable.animals_penguin, R.raw.animals_penguin));
        words.add(new Word(R.string.rabbit_english, R.string.rabbit_local, R.drawable.animals_rabbit, R.raw.animals_rabbit));
        words.add(new Word(R.string.squirrel_english, R.string.squirrel_local, R.drawable.animals_squirrel, R.raw.animals_squirrel));
        words.add(new Word(R.string.turtle_english, R.string.turtle_local, R.drawable.animals_turtle, R.raw.animals_turtle));
        words.add(new Word(R.string.wolf_english, R.string.wolf_local, R.drawable.animals_wolf, R.raw.animals_wolf));
        words.add(new Word(R.string.zebra_english, R.string.zebra_local, R.drawable.animals_zebra, R.raw.animals_zebra));
        words.add(new Word(R.string.capybara_english, R.string.capybara_local, R.drawable.animals_capybara, R.raw.animals_capybara));

        // creating word adapter
        WordAdapter itemsAdapter = new WordAdapter(this, words, R.color.category_animals, R.color.animals_image_background);
        ListView listView = (ListView) findViewById(R.id.words_list);
        listView.setAdapter(itemsAdapter);
        am = (AudioManager) this.getSystemService(Context.AUDIO_SERVICE);

        // set listener for playing music when item was clicked
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                int result = am.requestAudioFocus(afChangeListener,
                        AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN_TRANSIENT);
                if (result == AudioManager.AUDIOFOCUS_REQUEST_GRANTED) {
                    releaseMediaPlayer();
                    mp = MediaPlayer.create(AnimalsActivity.this, words.get(position).getAudioResourseId());
                    mp.start();
                    mp.setOnCompletionListener(completionListener);
                }
            }

        });


    }

    @Override
    protected void onStop() {
        super.onStop();
        releaseMediaPlayer();
    }

    private void releaseMediaPlayer() {
        if (mp != null) {
            mp.release();
            mp = null;
            am.abandonAudioFocus(afChangeListener);
        }
    }
}
