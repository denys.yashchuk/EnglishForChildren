package ua.denysyashchuk.englishforchildren;

import android.content.Intent;
import android.content.res.Configuration;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import java.util.Locale;

/**
 * This class used for opening other activities with the help of intents.
 */
public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // If Localization is not supported set language to ukrainian(uk)
        Log.wtf("Local", Locale.getDefault().toString());
        if (!Locale.getDefault().toString().equals("uk_UA")
                && !Locale.getDefault().toString().equals("ru_RU")) {
            String languageToLoad = "uk";
            Locale locale = new Locale(languageToLoad);
            Locale.setDefault(locale);
            Configuration config = new Configuration();
            config.locale = locale;
            getBaseContext().getResources().updateConfiguration(config,
                    getBaseContext().getResources().getDisplayMetrics());
        }
        setContentView(R.layout.activity_main);

        FrameLayout numbersFL = (FrameLayout) findViewById(R.id.numbers);
        FrameLayout familyFL = (FrameLayout) findViewById(R.id.family);
        FrameLayout colorsFL = (FrameLayout) findViewById(R.id.colors);
        FrameLayout animalsFL = (FrameLayout) findViewById(R.id.animals);

        numbersFL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, NumbersActivity.class);
                startActivity(i);
            }
        });

        colorsFL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, ColorsActivity.class);
                startActivity(i);
            }
        });

        familyFL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, FamilyActivity.class);
                startActivity(i);
            }
        });

        animalsFL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, AnimalsActivity.class);
                startActivity(i);
            }
        });


    }
}
