package ua.denysyashchuk.englishforchildren;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

/**
 * Extends ArrayAdapter<Word> for using in the list
 */
public class WordAdapter extends ArrayAdapter<Word> {

    private int categoryBackgroundColor;
    private int imageBackgroundColor;

    public WordAdapter(Context context, List<Word> objects, int categoryBackgroundColor, int imageBackgroundColor) {
        super(context, 0, objects);
        this.categoryBackgroundColor = categoryBackgroundColor;
        this.imageBackgroundColor = imageBackgroundColor;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Check if the existing view is being reused, otherwise inflate the view
        View listItemView = convertView;
        if (listItemView == null) {
            listItemView = LayoutInflater.from(getContext()).inflate(
                    R.layout.list_layout, parent, false);
        }
        Word word = getItem(position);

        // set text to the TextView
        TextView localTextView = (TextView) listItemView.findViewById(R.id.local_word);
        localTextView.setText(word.getLocalTranslation());

        // set text to the TextView
        TextView englishTextView = (TextView) listItemView.findViewById(R.id.english_word);
        englishTextView.setText(word.getEnglishTranslation());

        // set background for text
        View textContainer = listItemView.findViewById(R.id.text_container);
        int color = ContextCompat.getColor(getContext(), categoryBackgroundColor);
        textContainer.setBackgroundColor(color);

        // set image and background color for it
        ImageView image = (ImageView) listItemView.findViewById(R.id.image_view);
        image.setBackgroundColor(ContextCompat.getColor(getContext(), imageBackgroundColor));
        image.setImageResource(word.getImageResourseId());

        return listItemView;
    }

}
